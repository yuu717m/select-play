#!/usr/bin/env node

// play-x.x.x directory must be in /usr/local/src
"use strict";

let fs = require("fs");

function usage() {
    console.error("usage: play-select version");
    console.error("  ex.) eval `(play-select 1.4.3)`");
}

function output_env(play_path) {
    let paths = [];

    paths.push(play_path);
    process.env.PATH.split(":").forEach(function (path) {
        if (!path.match(/[/]play-*/)) {
            paths.push(path);
        }
    });

    console.log("PATH=" + paths.join(":"));
    console.log("PLAY_PATH=" + play_path);
    console.log("export PATH");
    console.log("export PLAY_PATH");
}

if (process.argv.length < 3) {
    usage();
    process.exit(64);   // EX_USAGE @ /usr/include/sysexits.h
}

(function main(argv) {
    let version = argv[0],
        play_path = "/usr/local/src/play-" + version;

    fs.exists(play_path, (exist) => {
        if (exist) {
            output_env(play_path);
        } else {
            console.error("Not found: " + play_path);
        }
    });
})(process.argv.slice(2));


