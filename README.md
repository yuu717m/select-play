# README #

### What is this repository for? ###

play framework の実行環境切り替えをサポートするスクリプト

前提:
    
play-x.x.x の配置場所が /usr/local/src であること

使用例:

    % eval `(select-play 1.4.3)`

### How do I get set up? ###

npm install -g
